# HOWTO

Create directory ```genomecruzer``` and copy Genome Cruzer application and data in it.

Build the image

```$ docker build -t your_image_name .```

Start container

Example with WEBPORT=5907 and DISPLAY=:7 is following

```docker run --init --runtime=nvidia --name=cruzer_container7 -e WEBPORT=5907 -e DISPLAY=":7" -e MYVNCPASSWORD="your_favourite_password" -v /tmp/.X11-unix/X0:/tmp/.X11-unix/X0 --rm -i -p 5907:5907 your_image_name```

